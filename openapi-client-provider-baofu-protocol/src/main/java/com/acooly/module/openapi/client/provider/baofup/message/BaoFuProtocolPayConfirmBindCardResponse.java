package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPResponse;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/4/13 14:29
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_PAY_CONFIRM_BIND_CARD,type = ApiMessageType.Response)
public class BaoFuProtocolPayConfirmBindCardResponse extends BaoFuPResponse {

    /**
     * 签约协议号
     * 只有成功时该字段才有值
     * 加密方式：Base64转码后，使用数字信封指定的方式和密钥加密
     */
    @BaoFuPAlias(value = "protocol_no",isDecode = true)
    private String protocolNo;

    /**
     * 银行编码
     * 只有在绑卡成功后该字段才有值
     */
    @BaoFuPAlias(value = "bank_code")
    private String bankCode;

    /**
     * 银行名称
     * 只有在绑卡成功后该字段才有值
     */
    @BaoFuPAlias(value = "bank_name")
    private String bankName;
}
