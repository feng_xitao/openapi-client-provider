package com.acooly.module.openapi.client.provider.newyl.message.xStream.batchDeduct.request;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author fufeng 2018/1/26 15:30.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("TRANS_DETAIL")
public class BdReqTransDetail {
    /**
     *记录序号
     */
    @XStreamAlias("SN")
    private String sn;
    /**
     *银联网络用户编号
     */
    @XStreamAlias("E_USER_CODE")
    private String eUserCode;
    /**
     *银行代码
     */
    @XStreamAlias("BANK_CODE")
    private String bankCode;
    /**
     *账号类型
     */
    @XStreamAlias("ACCOUNT_TYPE")
    private String accountType;
    /**
     *账号
     */
    @XStreamAlias("ACCOUNT_NO")
    private String accountNo;
    /**
     *账号名
     */
    @XStreamAlias("ACCOUNT_NAME")
    private String accountName;
    /**
     *开户行所在省
     */
    @XStreamAlias("PROVINCE")
    private String province;
    /**
     *开户行所在市
     */
    @XStreamAlias("CITY")
    private String city;
    /**
     *开户行名称
     */
    @XStreamAlias("BANK_NAME")
    private String bankName;
    /**
     *账号属性
     */
    @XStreamAlias("ACCOUNT_PROP")
    private String accountProp;
    /**
     *金额
     */
    @XStreamAlias("AMOUNT")
    private String amount;
    /**
     *货币类型
     */
    @XStreamAlias("CURRENCY")
    private String currency;
    /**
     *协议号
     */
    @XStreamAlias("PROTOCOL")
    private String protocol;
    /**
     *协议用户编号
     */
    @XStreamAlias("PROTOCOL_USERID")
    private String protocolUserId;
    /**
     *开户证件类型
     */
    @XStreamAlias("ID_TYPE")
    private String idType;
    /**
     *证件号
     */
    @XStreamAlias("ID")
    private String id;
    /**
     *手机号/小灵通
     */
    @XStreamAlias("TEL")
    private String tel;
    /**
     *清算账号
     */
    @XStreamAlias("RECKON_ACCOUNT")
    private String reckonAccount;
    /**
     *自定义用户号
     */
    @XStreamAlias("CUST_USERID")
    private String custUserId;
    /**
     *备注
     */
    @XStreamAlias("REMARK")
    private String remark;



}
