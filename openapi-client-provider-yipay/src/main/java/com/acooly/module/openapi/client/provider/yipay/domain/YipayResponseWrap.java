/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-01-23 16:50 创建
 */
package com.acooly.module.openapi.client.provider.yipay.domain;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhangpu 2018-01-23 16:50
 */
@Getter
@Setter
public class YipayResponseWrap {

    /**
     * 接入商户标志（商户号:merchantNo）
     */
    private String success;

    private String errorCode;

    private String errorMsg;

    private String sign;

    private String result;
}
