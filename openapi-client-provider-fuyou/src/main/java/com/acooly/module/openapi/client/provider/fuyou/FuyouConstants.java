/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.fuyou;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;

/**
 * @author zhangpu 2017-09-17 17:49
 */
public class FuyouConstants {

    public static final String SIGNATURE = "SIGN";
    /**
     * 签名实现KEY和秘钥
     */
    public static final String SIGNER_KEY = "fuyou";

    public static final String MD5_SIGN_TYPE = "MD5";

    public static final String NETBANK_NAME = "富友网关";

    public static String getCanonicalUrl(String gatewayUrl, String serviceName) {
        String serviceUrl = gatewayUrl;
        serviceUrl = Strings.removeEnd(serviceUrl, "/");
        if (!Strings.startsWith(serviceName, "/")) {
            serviceUrl = serviceUrl + "/" + serviceName;
        } else {
            serviceUrl = serviceUrl + serviceName;
        }
        return serviceUrl;
    }

    public static String getServiceName(ApiMessage apiMessage) {
        if (Strings.isNotBlank(apiMessage.getService())) {
            return apiMessage.getService();
        }

        FuyouApiMsgInfo apiMsgInfo = getApiMsgInfo(apiMessage);
        if (apiMsgInfo != null && apiMsgInfo.service() != null) {
            return apiMsgInfo.service().code();
        }
        throw new RuntimeException("请求报文的service为空");
    }

    public static FuyouApiMsgInfo getApiMsgInfo(ApiMessage apiMessage) {
        return apiMessage.getClass().getAnnotation(FuyouApiMsgInfo.class);
    }
}
