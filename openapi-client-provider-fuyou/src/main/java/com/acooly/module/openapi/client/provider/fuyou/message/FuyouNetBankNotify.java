package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouNotify;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.acooly.module.openapi.client.provider.fuyou.support.FuyouAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_NETBANK,type = ApiMessageType.Notify)
public class FuyouNetBankNotify extends FuyouNotify{

    /**
     * 商户代码
     * 富友分配给各合作商户的唯一识别码
     */
    @FuyouAlias("mchnt_cd")
    @NotBlank
    @Size(max = 15)
    private String mchntCd;

    /**
     * 商户订单号
     * 客户支付后商户网站产生的一个唯
     * 一的定单号，该订单号应该在相当
     * 长的时间内不重复。富友通过订单
     * 号来唯一确认一笔订单的重复性.
     */
    @FuyouAlias("order_id")
    @NotBlank
    @Size(max = 30)
    private String orderId;

    /**
     * 订单日期
     * YYYYMMDD
     */
    @FuyouAlias("order_date")
    @NotBlank
    @Size(max = 8)
    private String orderDate;

    /**
     * 交易金额
     * 客户支付订单的总金额，一笔订单
     * 一个，以分为单位。不可以为零，
     * 必需符合金额标准。
     */
    @FuyouAlias("order_amt")
    @NotBlank
    @Size(max = 12)
    private String orderAmt;

    /**
     * 订单状态
     * ‘00’ – 订单已生成(初始状态)
     * ‘01’ – 订单已撤消
     * ‘02’ – 订单已合并
     * ‘03’ – 订单已过期
     * ‘04’ – 订单已确认(等待支付)
     * ‘05’ – 订单支付失败
     * ‘11’ – 订单已支付
     */
    @FuyouAlias("order_st")
    @NotBlank
    @Size(max = 2)
    private String orderSt;

    /**
     * 交易代码
     * ‘B01’- 借贷合一
     * ‘B21’- 借记卡
     * ‘B02’- 企业网银
     * ‘B03’- 借贷合一快捷支付
     * ‘B07’- 借记卡快捷支付
     * 选填,不参与MD5 验签
     */
    @FuyouAlias("txn_cd")
    @Size(max = 3)
    private String txnCd;


    /**
     * 错误代码
     * 0000 表示成功 其他失败
     */
    @FuyouAlias("order_pay_code")
    @NotBlank
    @Size(max = 4)
    private String orderPayCode;

    /**
     * 错误中文描述
     */
    @FuyouAlias("order_pay_error")
    @NotBlank
    @Size(max = 40)
    private String orderPayError;

    /**
     * 保留字段
     */
    @FuyouAlias("resv1")
    @NotBlank
    @Size(max = 40)
    private String resv1;

    /**
     * 富友流水号
     * 供商户查询支付交易状态及对账用
     */
    @FuyouAlias("fy_ssn")
    @NotBlank
    @Size(max = 12)
    private String fySsn;

    @Override
    public String getSignStr() {
        return getMchntCd()+"|"+getOrderId()+"|"+getOrderDate()+"|"+getOrderAmt()+"|"+getOrderSt()+"|"
                +getOrderPayCode()+"|"+getOrderPayError()+"|"+getResv1()+"|"+getFySsn()+"|";
    }

}
