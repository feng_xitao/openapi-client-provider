package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouResponse;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/3/19 15:51
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FIRST_QUICKPAY_CREATE_ORDER,type = ApiMessageType.Response)
@XStreamAlias("RESPONSE")
public class FuyouFirstQuickCreateOrderResponse extends FuyouResponse {

    /**
     * 交易类型
     */
    @XStreamAlias("TYPE")
    private String type = "03";

    /**
     * 商户订单号
     * 商户订单流水号商户确保唯一
     */
    @Size(max = 60)
    @NotBlank
    @XStreamAlias("MCHNTORDERID")
    private String merchOrderNo;

    /**
     * 用户编号
     * 商户端用户的唯一编号，即用户 ID
     */
    @XStreamAlias("USERID")
    @NotBlank
    @Size(max = 40)
    private String userId;

    /**
     * 富友订单号
     * 富友生成的订单号，该订单号在相
     当长的时间内不重复。富友通过订
     单号来唯一确认一笔订单的重复性
     */
    @XStreamAlias("ORDERID")
    private String bankOrderId;

    /**
     * 交易金额
     * 交易金额，分为单位
     */
    @XStreamAlias("AMT")
    @NotBlank
    @Size(max = 12)
    private String amount;

    /**
     * 保留字段 1
     */
    @XStreamAlias("REM1")
    @NotBlank
    @Size(max = 256)
    private String remOne;

    /**
     * 保留字段 2
     */
    @XStreamAlias("REM2")
    @NotBlank
    @Size(max = 256)
    private String remTwo;

    /**
     * 保留字段 3
     */
    @XStreamAlias("REM3")
    @NotBlank
    @Size(max = 256)
    private String remThree;

    /**
     * 签名类型
     */
    @XStreamAlias("SIGNTP")
    private String signType;

    /**
     * 摘要数据
     */
    @XStreamAlias("SIGN")
    private String sign;

    /**
     * 支付接口所
     * 需字段
     * 调支付接口时原样传送该字段
     */
    @XStreamAlias("SIGNPAY")
    private String signPay;

    @Override
    public String getSignStr() {
        return getType()+"|"+getVersion()+"|"+getResponseCode()+"|"+getPartner()+"|"+getMerchOrderNo()+"|"+getUserId()+"|"+getBankOrderId()+"|"+getAmount()+"|";
    }
}
