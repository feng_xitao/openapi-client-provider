/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-16 16:11 创建
 */
package com.acooly.module.openapi.client.provider.hx;

import com.acooly.core.utils.Strings;

import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.acooly.module.openapi.client.provider.hx.HxProperties.PREFIX;


/**
 * @author zhangpu@acooly.cn
 */
@ConfigurationProperties(prefix = PREFIX)
public class HxProperties {
    public static final String PREFIX = "acooly.openapi.client.hx";

    /**
     * 网关地址
     */
    private String gatewayUrl;

    /**
     * 向量
     */
    private String deskey;

    /**
     * 密钥
     */
    private String desiv;

    /**
     * 解密MD5串
     */
    private String MD5Key;

    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    /**
     * 报文中签名字段名称
     */
    private String sign;

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;

    /**
     * 商户号
     */
    private String partnerId;

    /**
     * WEB充值同步通知地址
     */
    private String depositReturnUrl;

    /**
     * 业务代码
     */
    private String businessCode;

    /**
     * 商户名
     */
    private String sellerName;

    /**
     * 异步URL
     */
    private String notifyUrl = "/hx/sdk/notify";


    public String getReturnUrl(String url) {
        return getCanonicalUrl(this.domain, url);
    }


    protected String getCanonicalUrl(String prefix, String postfix) {
        if (Strings.endsWith(prefix, "/")) {
            prefix = Strings.removeEnd(prefix, "/");
        }
        if (Strings.startsWith(postfix, "/")) {
            return prefix + postfix;
        } else {
            return prefix + "/" + postfix;
        }
    }

    public String getDeskey() {
        return deskey;
    }

    public void setDeskey(String deskey) {
        this.deskey = deskey;
    }

    public String getDesiv() {
        return desiv;
    }

    public void setDesiv(String desiv) {
        this.desiv = desiv;
    }

    public String getMD5Key() {
        return MD5Key;
    }

    public void setMD5Key(String MD5Key) {
        this.MD5Key = MD5Key;
    }

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public long getConnTimeout() {
        return connTimeout;
    }

    public void setConnTimeout(long connTimeout) {
        this.connTimeout = connTimeout;
    }

    public long getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getDepositReturnUrl() {
        return depositReturnUrl;
    }

    public void setDepositReturnUrl(String depositReturnUrl) {
        this.depositReturnUrl = depositReturnUrl;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getBusinessCode() {
        return businessCode;
    }

    public void setBusinessCode(String businessCode) {
        this.businessCode = businessCode;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }


}
