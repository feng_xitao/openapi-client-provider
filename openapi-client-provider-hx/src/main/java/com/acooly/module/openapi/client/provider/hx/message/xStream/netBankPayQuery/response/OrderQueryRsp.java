package com.acooly.module.openapi.client.provider.hx.message.xStream.netBankPayQuery.response;

import com.acooly.module.openapi.client.provider.hx.message.xStream.common.RespHead;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Data;

/**
 * @author fufeng 2018/3/2 15:50.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("OrderQueryRsp")
public class OrderQueryRsp {

    @XStreamAlias("head")
    private RespHead head;

    @XStreamAlias("body")
    private RespNetBankQueryBody body;


}
