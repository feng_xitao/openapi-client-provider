/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.webank.domain;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.webank.WeBankConstants;


/**
 * @author
 */
public class WeBankApiMessage implements ApiMessage {

    /**
     * 接口名称
     */
    private String bizType;
    /**
     * 商户号
     */
    private String partnerId;

    /**
     * 签名
     * RSA签名字符串，再用Base64编码
     */
    @ApiItem(sign = false)
    private String sign;

    /**
     * 签名类型
     */
    private String sign_type = WeBankConstants.SIGN_TYPE;

    /**
     * 请求url
     */
    @ApiItem(sign = false)
    private String gatewayUrl;

    public void doCheck() {

    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    @Override
    public String getService() {
        return bizType;
    }

    @Override
    public String getPartner() {
        return partnerId;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

    public String getGatewayUrl() {
        return gatewayUrl;
    }

    public void setGatewayUrl(String gatewayUrl) {
        this.gatewayUrl = gatewayUrl;
    }

    public String getBizType() {
        return bizType;
    }

    public void setBizType(String bizType) {
        this.bizType = bizType;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getSign_type() {
        return sign_type;
    }

    public void setSign_type(String sign_type) {
        this.sign_type = sign_type;
    }
}
