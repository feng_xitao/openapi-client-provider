package com.acooly.module.openapi.client.provider.webank.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankApiMsgInfo;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankResponse;
import com.acooly.module.openapi.client.provider.webank.enums.WeBankServiceEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@WeBankApiMsgInfo(service = WeBankServiceEnum.WEBANK_DEDUCT, type = ApiMessageType.Response)
public class WeBankDeductResponse extends WeBankResponse {

    /**
     * 商户号
     */
    String merId;

    /**
     * 订单号
     */
    String orderId;

    /**
     * 交易类型
     */
    String bizType;

}
