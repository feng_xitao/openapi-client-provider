package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuRequest;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/1/30 17:43
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.BILL_DOWNLOAD, type = ApiMessageType.Request)
public class BaoFuBillDowloadRequest extends BaoFuRequest {

    /**
     * 文件类型 收单：fi  出款：fo
     */
    @NotBlank(message = "文件类型不能为空")
    @BaoFuAlias(value = "file_type")
    private String fileType;


    /**
     * 客户端IP 商户下载文件所属IP须向宝付报备
     */
    @NotBlank(message = "客户端IP不能为空")
    @BaoFuAlias(value = "client_ip")
    private String clientIp;


    /**
     * 清算日期 对账单日期
     * 格式：yyyy-mm-dd
     */
    @NotBlank(message = "服务版本不能为空")
    @BaoFuAlias(value = "settle_date")
    private String settleDate;

    /**
     * 文件路径
     */
    @BaoFuAlias(value = "filePath", sign = false, request = false)
    private String filePath;

    /**
     * 文件名称，如果不传默认为：账期.text
     */
    @BaoFuAlias(value = "fileName", sign = false, request = false)
    private String fileName;

    /**
     * 文件后缀
     */
    @BaoFuAlias(value = "fileSuffix", sign = false, request = false)
    private String fileSuffix = "zip";
}
