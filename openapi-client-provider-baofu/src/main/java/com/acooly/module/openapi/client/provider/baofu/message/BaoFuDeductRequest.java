package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuRequest;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuDeductBankCodeEnum;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/1/26 13:56
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.DEDUCT,type = ApiMessageType.Request)
public class BaoFuDeductRequest extends BaoFuRequest {

    /**
     * 接入类型 默认0000,表示为储蓄卡支付。
     */
    @NotBlank
    @BaoFuAlias(value = "biz_type", request = false)
    private String bizType;

    /**
     * 银行编码 BaoFuDeductBankCodeEnum
     */
    @NotBlank
    @BaoFuAlias(value = "pay_code", request = false)
    private String payCode;

    /**
     * 安全标识
     * 默认为：2
     *1:不进行信息严格验证
     *2:对四要素（身份证号、持卡人姓名、银行卡绑定手机、卡号）进行严格校验
     */
    @BaoFuAlias(value = "pay_cm", request = false)
    private String payCm;

    /**
     * 卡号
     * 整型数字
     * 提交给宝付的银行卡卡号
     */
    @NotBlank
    @BaoFuAlias(value = "acc_no", request = false)
    private String accNo;

    /**
     * 身份证类型
     * 固定值：01
     * 01认为身份证号
     */
    @BaoFuAlias(value = "id_card_type", request = false)
    private String idCardType = "01";

    /**
     * 身份证号 提交给宝付的持卡人身份证号
     */
    @NotBlank
    @BaoFuAlias(value = "id_card", request = false)
    private String idCard;

    /**
     * 持卡人姓名 提交给宝付的持卡人姓名
     */
    @NotBlank
    @BaoFuAlias(value = "id_holder", request = false)
    private String idHolder;

    /**
     * 银行卡绑定手机号 提交给宝付的持卡人预留手机号（四要素验证时必传）
     */
    @BaoFuAlias(value = "mobile", request = false)
    private String mobile;

    /**
     * 卡有效期 格式：YYMM
     * 如：07月/18年则写成1807
     * 信用卡时必传
     */
    @BaoFuAlias(value = "valid_date", request = false)
    private String validDate;

    /**
     * 卡安全码 银行卡背后最后三位数字
     */
    @BaoFuAlias(value = "valid_no", request = false)
    private String validNo;

    /**
     * 商户订单号
     * 唯一订单号，8-50 位字母和数字，
     * 注：支付请求中trans_id作为主键，如果有重复的交易请求存在，以第一个成功交易为准，后续重复交易不被受理。
     * 不支持商户提交重复订单号，对重复订单号将直接提示订单已经提交。
     */
    @NotBlank
    @BaoFuAlias(value = "trans_id", request = false)
    private String transId;

    /**
     * 交易金额 单位：分
     */
    @NotBlank
    @BaoFuAlias(value = "txn_amt", request = false)
    private String txnAmt;

    /**
     * 订单日期 14 位定长。
     * 格式：年年年年月月日日时时分分秒秒
     */
    @NotBlank
    @BaoFuAlias(value = "trade_date", request = false)
    private String tradeDate;

    /**
     * 请求方保留域
     */
    @BaoFuAlias(value = "req_reserved")
    private String reqReserved;

    /**
     * 附加字段
     */
    @BaoFuAlias(value = "additional_info")
    @Size(max = 128)
    private String additionalInfo;

    /**
     * 商户流水号 8-20 位字母和数字，每次请求都不可重复
     *
     */
    @BaoFuAlias(value = "trans_serial_no",request = false)
    private String transSerialNo = Ids.Did.getInstance().getId(20);

    @Override
    public void doCheck() {
        super.doCheck();
//        BaoFuDeductBankCodeEnum deductBankCode = BaoFuDeductBankCodeEnum.find(getPayCode());
//        if(deductBankCode == null) {
//            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(),"不支持此银行");
//        }
    }
}
