package com.acooly.module.openapi.client.provider.baofu.message;

import com.acooly.core.common.exception.OrderCheckException;
import com.acooly.core.common.facade.ResultCode;
import com.acooly.core.utils.Ids;
import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofu.domain.BaoFuRequest;
import com.acooly.module.openapi.client.provider.baofu.enums.BaoFuServiceEnum;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuBatchDeductInfo;
import com.acooly.module.openapi.client.provider.baofu.message.info.BaoFuWithdrawRequestInfo;
import com.acooly.module.openapi.client.provider.baofu.support.BaoFuAlias;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * @author zhike 2018/1/29 16:11
 */
@Getter
@Setter
@BaoFuApiMsgInfo(service = BaoFuServiceEnum.BATCH_DEDUCT,type = ApiMessageType.Request)
public class BaoFuBatchDeductRequest extends BaoFuRequest{

    /**
     * 商户流水号 8-20 位字母和数字，每次请求都不可重复
     *
     */
    @BaoFuAlias(value = "trans_serial_no",request = false)
    private String transSerialNo = Ids.Did.getInstance().getId(20);

    /**
     * 商户批次号 商户请求批次号，每次请求都不可重复
     *
     */
    @BaoFuAlias(value = "trans_batch_id",request = false)
    private String transBatchId;

    /**
     * 代扣数据域
     * 实际信息 最多支持5000条
     */
    @BaoFuAlias(value = "actual_info",request = false)
    private List<BaoFuBatchDeductInfo> baoFuBatchDeductInfos;

    @Override
    public void doCheck() {
        super.doCheck();
        if(baoFuBatchDeductInfos == null) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(),"批量代扣申请不能为空");
        }else if(baoFuBatchDeductInfos.size() > 5000) {
            throw new OrderCheckException(ResultCode.PARAMETER_ERROR.getCode(),"批量代扣申请数量不能一次超过5000条");
        }
        for(BaoFuBatchDeductInfo baoFuBatchDeductInfo:baoFuBatchDeductInfos) {
            Validators.assertJSR303(baoFuBatchDeductInfo);
        }
    }
}
