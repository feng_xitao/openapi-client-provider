/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月29日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_VERIFY, type = ApiMessageType.Response)
public class QueryVerifyResponse extends SinapayResponse {

	/** 认证内容 */
	@Size(max = 30)
	@NotEmpty
	@ApiItem(value = "verify_entity", securet = true)
	private String verifyEntity;

	/** 认证时间 格式为：年[4位]月[2位]日[2位]时[2位]分[2位]秒[2位] */
	@Size(max = 14)
	@NotEmpty
	@ApiItem("verify_time")
	private String verifyTime;

	@Size(max = 200)
	@ApiItem("extend_param")
	private String extendParam;

}
