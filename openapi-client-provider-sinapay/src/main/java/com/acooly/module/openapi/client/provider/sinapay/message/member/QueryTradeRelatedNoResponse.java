package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/7/10 15:30
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_TRADE_RELATED_NO, type = ApiMessageType.Response)
public class QueryTradeRelatedNoResponse extends SinapayResponse {

    /**
     *内部交易关联号
     * 我司内部交易关联号
     */
    @Size(max = 64)
    @ApiItem(value = "inner_trade_related_no")
    private String innerTradeRelatedNo;

    /**
     *可代付金额
     * 单位为：RMB Yuan。精确到小数点后两位。
     */
    @MoneyConstraint
    @ApiItem(value = "balance")
    private Money balance;
}
