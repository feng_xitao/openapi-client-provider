package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/8/6 11:19
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_SINGLE_HOSTING_PAY_TRADE, type = ApiMessageType.Notify)
public class CreateSingleHostingPayTradeNotify extends TradeNotify{
}
