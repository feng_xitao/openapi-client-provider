package com.acooly.module.openapi.client.provider.yinsheng.message.dto;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/2/8 14:04
 */
@Getter
@Setter
public class YinShengWithdrawD0Info implements Serializable {
    /**
     * 店铺时间YYYYMMdd
     */
    @NotBlank
    private String shopdate;
    /**
     * 商户订单号
     */
    @NotBlank
    private String out_trade_no;

    /**
     * 商户号
     * 1.当商户号为空时，默认将partner_id,作为商户号
     * 2.如果提现的商户号和合作商户号不一致则需要检查机构信息
     */
    private String merchant_usercode;

    /**
     *暂时只支持币种：CNY（人民币）
     */
    private String currency = "CNY";

    /**
     * 提现的总金额。单位为：RMB Yuan。取值范围为[0.01，99999999.99]，精确到小数点后两位。Number(10,2)指10位长度，2位精度
     */
    @NotBlank
    @Size(min = 1,max = 10)
    private String total_amount;

    /**
     * 订单说明
     */
    private String subject;
}
