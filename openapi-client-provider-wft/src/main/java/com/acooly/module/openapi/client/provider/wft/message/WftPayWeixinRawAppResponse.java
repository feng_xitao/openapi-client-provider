package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftResponse;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2017/11/29 15:11
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.PAY_WEIXIN_RAW_APP, type = ApiMessageType.Response)
public class WftPayWeixinRawAppResponse extends WftResponse{

    /**
     * --------------------------以下字段在 status 和 result_code 都为 0的时候有返回--------------------------------
     */

    /**
     * 支付信息
     * Json格式的字符串，微信官方SDK中需要的参数信息
     */
    @WftAlias("pay_info")
    private String payInfo;

    /**
     * 商户订单号
     */
    @WftAlias(value = "out_trade_no")
    private String outTradeNo;

    /**
     * 平台订单号
     */
    @WftAlias(value = "transaction_id")
    private String transactionId;
}
