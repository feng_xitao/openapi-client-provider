package com.acooly.module.openapi.client.provider.wft.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wft.domain.WftApiMsgInfo;
import com.acooly.module.openapi.client.provider.wft.domain.WftRequest;
import com.acooly.module.openapi.client.provider.wft.domain.WftResponse;
import com.acooly.module.openapi.client.provider.wft.enums.WftBillTypeEnum;
import com.acooly.module.openapi.client.provider.wft.enums.WftServiceEnum;
import com.acooly.module.openapi.client.provider.wft.support.WftAlias;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Size;

/**
 * @author zhike 2017/12/11 18:05
 * <p>
 * service接口类型参数值：（与传入的mch_id的类型对应）pay.bill.merchant：下载单个商户时的对账单
 * pay.bill.bigMerchant：下载大商户下所有子商户的对账单
 * pay.bill.agent：下载某渠道下所有商户的对账单
 */
@Getter
@Setter
@WftApiMsgInfo(service = WftServiceEnum.BILL_DOWNLOAD, type = ApiMessageType.Response)
public class WftBillDownloadResponse extends WftResponse {

}
