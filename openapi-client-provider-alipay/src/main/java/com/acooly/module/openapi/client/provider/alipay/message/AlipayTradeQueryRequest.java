package com.acooly.module.openapi.client.provider.alipay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayApiMsgInfo;
import com.acooly.module.openapi.client.provider.alipay.domain.AlipayRequest;
import com.acooly.module.openapi.client.provider.alipay.enums.AlipayServiceEnum;

import lombok.Getter;
import lombok.Setter;

@AlipayApiMsgInfo(service=AlipayServiceEnum.PAY_ALIPAY_TRADE_APP,type=ApiMessageType.Request)
@Setter
@Getter
public class AlipayTradeQueryRequest extends AlipayRequest{

	/**
	 * 支付宝交易号
	 */
	private String tradeNo;
	
}
