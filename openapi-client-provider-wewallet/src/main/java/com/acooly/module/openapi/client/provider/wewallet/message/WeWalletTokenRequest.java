package com.acooly.module.openapi.client.provider.wewallet.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletApiMsgInfo;
import com.acooly.module.openapi.client.provider.wewallet.domain.WeWalletRequest;
import com.acooly.module.openapi.client.provider.wewallet.enums.WeWalletServiceEnum;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@WeWalletApiMsgInfo(service = WeWalletServiceEnum.WEWALLET_ACCESS_TOKEN, type = ApiMessageType.Request)
public class WeWalletTokenRequest extends WeWalletRequest {

    /**
     * 微众分配平台标识
     */
    private String appId;
    /**
     * 微众分配的 app_id 密钥
     */
    private String secret;
    /**
     * 授权类型，默认值：
     client_credential
     */
    private String grantType;
    /**
     * 版本号，默认值：1.0.0
     */
    private String version;

}
