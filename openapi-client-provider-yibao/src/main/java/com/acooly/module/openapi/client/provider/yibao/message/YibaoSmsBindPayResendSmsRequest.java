package com.acooly.module.openapi.client.provider.yibao.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoAlias;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoApiMsg;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoRequest;
import com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/6/26 14:31
 */
@Getter
@Setter
@YibaoApiMsg(service = YibaoServiceNameEnum.YIBAO_BINDPAY_RESENDSMS,type = ApiMessageType.Request)
public class YibaoSmsBindPayResendSmsRequest extends YibaoRequest {

    /**
     * 建议短验发送类型
     * MESSAGE：短验码将以短信的方式发送给用户
     * VOICE：短验码将以语音的方式发送给用户
     * 默认值为上次请求的建议发送类型
     */
    @YibaoAlias(value = "advicesmstype")
    private String adviceSmsType;
}
