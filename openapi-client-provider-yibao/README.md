<!-- title: 易宝支付SDK组件 -->
<!-- type: gateway -->
<!-- author: zhike -->

## 1. 提供能力

组件的集成模式为标准组件集成方式。直接通过pom文件引入依赖并配置对应的参数即可。

## 2. 使用说明

```xml
<dependency>
  <groupId>com.acooly</groupId>
  <artifactId>openapi-client-provider-yibao</artifactId>
  <version>4.2.0-SNAPSHOT</version>
</dependency>
```

直接采用yibao的SDK，然后扩展异步通知处理
使用方式：
1、在自己工程注入YibaoApiService

2、此sdk提供翼支付全网运营商三要素验证、银行卡三要素验证、银行卡四要素验证、全网实时位置验证接口、运营商实时城市位置核验接口、运营商在网状态查询、运营商入网时长查询、电信运营商二要素验证(手机号+身份证)、电信运营商二要素验证(手机号+姓名)、身份证二要素验证接口、代扣、代扣订单查询、代扣异步通知、对账下载

3、密钥加载支持两种方式
   a：通过配置文件

4、本sdk中代扣支付的异步通知地址固定值，所以调用的时候不需要传入notifyUrl
   接收异步通知的时候实现接口NotifyHandler，重新handleNotify和serviceKey方法，其中serviceKey方法需要绑定的key值在枚举YibaoServiceNameEnum可以获取到
   代扣支付异步通知接收示例：
```java
  @Slf4j
  @Service
  public class YibaoDeductNotifyService implements NotifyHandler {
  
      @Override
      public void handleNotify(ApiMessage notify) {
          YibaoDeductNotify yibaoDeductNotify = (YibaoDeductNotify) notify;
          log.info("翼支付异步通知报文:{}", JSON.toJSONString(yibaoDeductNotify));
      }
  
      @Override
      public String serviceKey() {
          return YibaoServiceNameEnum.DEDUCT.getCode();
      }
  }
```
5、接口的服务码和对应的唯一表示统一放在com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum

注意：调用接口的时候method不用手动传入，如果是配置文件方式partnerId也不用传，sign_type默认为RSA,version默认为4.0.0.0，charset默认为UTF-8
     由于每个接口的请求地址可能不一样，配置文件里面配的地址可能不是当前接口的请求地址，所以SDK是支持手动传入请求地址的，判断逻辑为优先使用传入的请求
     地址，如果传入的请求地址为空，则使用配置文件的请求地址；需要注意的是在请求同步返回的时候如果接收到异常：ApiClientSocketTimeoutException、ApiClientProcessingException
     的时候，同步视为处理中，通过订单调用查询接口确认订单的最终状态


6、宝付接口对应使用的其它信息如终端号SDK也优先使用传入的值，如果传入的值为空，则使用配置文件中的值，尤其对托管模式的接入需要注意

7、配置文件示例：
## yibao
acooly.openapi.client.yibao.enable=true
acooly.openapi.client.yibao.merchantNo=3178000009051622
acooly.openapi.client.yibao.gatewayUrl=http://116.228.151.160:18002/
acooly.openapi.client.yibao.downloadUrl=
acooly.openapi.client.yibao.keystore=classpath:keystore/test/yibao/merchant.p12
acooly.openapi.client.yibao.keystorePswd=81024138
acooly.openapi.client.yibao.gatewayCert=classpath:keystore/test/yibao/bestpay.cer
acooly.openapi.client.yibao.aesKey=4f66405c4f66405c
acooly.openapi.client.yibao.aesVi=QUJDREVGR0hJS0w=
acooly.openapi.client.yibao.domain=http://218.70.106.250:8881
acooly.openapi.client.yibao.notifyUrl=/openapi/client/yibao/notify
acooly.openapi.client.yibao.connTimeout=10000
acooly.openapi.client.yibao.readTimeout=30000

##sftp下载对账文件配置
acooly.openapi.client.yibao.checkfile.host=acooly.cn
acooly.openapi.client.yibao.checkfile.port=23987
acooly.openapi.client.yibao.checkfile.username=mysftp
acooly.openapi.client.yibao.checkfile.password=Zp123456
acooly.openapi.client.yibao.checkfile.serverRoot=/upload
acooly.openapi.client.yibao.checkfile.localRoot=D:/download