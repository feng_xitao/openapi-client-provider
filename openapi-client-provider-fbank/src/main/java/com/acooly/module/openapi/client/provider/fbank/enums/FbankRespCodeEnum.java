/*
 * ouwen@yiji.com Inc.
 * Copyright (c) 2017 All Rights Reserved.
 * create by zhike
 * date:2017-03-30
 *
 */
package com.acooly.module.openapi.client.provider.fbank.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * inst_channel_api DebitCreditType 枚举定义
 *
 * @author zhikeRESP_CODE_ Date(" Date","2017-03-30 15:12:06"),
 */
public enum FbankRespCodeEnum implements Messageable {
    RESP_CODE_10000("10000", "success:交易成功（或退款成功）"),
    RESP_CODE_10001("10001", "mchntOrderNo_is_null:mchntOrderNo字段未传或传空值"),
    RESP_CODE_10002("10002", "mchntOrderNo_length_is_too_long:mchntOrderNo值长度超过64位"),
    RESP_CODE_10003("10003", "mchntId_is_null:mchntId字段未传或传空值"),
    RESP_CODE_10004("10004", "mchntId_length_is_too_long:mchntId值长度超16位"),
    RESP_CODE_10005("10005", "payPowerId_is_null:payPowerId字段未传或传空值"),
    RESP_CODE_10006("10006", "payPowerId_length_is_too_long:payPowerId值长度超12位"),
    RESP_CODE_10007("10007", "amount_is_null:amount字段未传或传空值"),
    RESP_CODE_10008("10008", "amount_length_is_too_long:amount值长度超12位"),
    RESP_CODE_10009("10009", "amount_value_less_than_zero:amount值不能小于等于0，最低1分"),
    RESP_CODE_10010("10010", "amount_value_is_not_an_integer:amount值必须为正整数，最低1分"),
    RESP_CODE_10011("10011", "subject_is_null:subject字段未传或传空值"),
    RESP_CODE_10012("10012", "subject_length_is_too_long:subject值长度超64位"),
    RESP_CODE_10013("10013", "version_is_null:version字段未传或传空值"),
    RESP_CODE_10014("10014", "version_length_is_too_long:version值长度超32位"),
    RESP_CODE_10015("10015", "notifyUrl_length_is_too_long  :notifyUrl值长度超128位"),
    RESP_CODE_10016("10016", "returnUrl_length_is_too_long  :returnUrl值长度超128位"),
    RESP_CODE_10017("10017", "subOpenId_is_null  :当payPowerId所传值为3、 6、 10时，必须传subOpenId字段"),
    RESP_CODE_10018("10018", "subOpenId_length_is_too_long  :subOpenId值长度超64位"),
    RESP_CODE_10019("10019", "cpChannel_length_is_too_long  :cpChannel值长度超256位"),
    RESP_CODE_10020("10020", "description_length_is_too_long  :description值长度超512位"),
    RESP_CODE_10021("10021", "extra_length_is_too_long:extra值长度超256位"),
    RESP_CODE_10022("10022", "body_length_is_too_long:body值长度超256位"),
    RESP_CODE_10023("10023", "currency_length_is_too_long:currency值长度超3位"),
    RESP_CODE_10024("10024", "mchid_is_invalid:mchntId字段值无效"),
    RESP_CODE_10026("10026", "power_is_not_exist:根据商户所传payPowerId值，未找到对应支付功能"),
    RESP_CODE_10027("10027", "power_not_open  :商户所传payPowerId值对应的支付功能未开通"),
    RESP_CODE_10028("10028", "merchant_sign_fail:商户下单请求签名验签失败"),
    RESP_CODE_10029("10029", "merchant_no_configure_relation  :商户未开通对应的支付功能，联系支付平台运营人员排查"),
    RESP_CODE_10030("10030", "merchant_order_already_exists  :商户所传mchntOrderNo商户订单号非代支付状态"),
    RESP_CODE_10031("10031", "order_insert_fail  :订单插入失败，联系支付平台技术支持排查"),
    RESP_CODE_10032("10032", "merchant_rate_not_exist:商户支付功能未配置费率，联系支付平台运营人员排查"),
    RESP_CODE_10033("10033", "merchant_get_rate_type_fail  :商户支付功能所配置的费率类型未知"),
    RESP_CODE_10034("10034", "merchant_not_configure_parameter  :商户支付功能未配置支付参数"),
    RESP_CODE_10035("10035", "system_nonsupport_this_power  :程序暂时未开发该支付功能"),
    RESP_CODE_10036("10036", "update_order_fail:订单更新失败"),
    RESP_CODE_10037("10037", "merchant_prepay_exception:预下单程序异常"),
    RESP_CODE_10038("10038", "alipay_precreate_response_is_null  :支付宝扫码支付请求上游失败"),
    RESP_CODE_10039("10039", "alipay_trade_precreate_fail  :支付宝扫码支付请求上游成功，但预下单失败"),
    RESP_CODE_10040("10040", "alipay_trade_precreate_exception  :支付宝扫码支付程序异常"),
    RESP_CODE_10041("10041", "alipay_pay_response_is_null  :支付宝条码支付请求上游失败"),
    RESP_CODE_10042("10042", "alipay_order_closed  :支付宝条码支付时，扣款失败，调用查询接口，订单状态已关闭"),
    RESP_CODE_10043("10043", "alipay_order_finished  :支付宝条码支付时，扣款失败，调用查询接口，订单状态已结束"),
    RESP_CODE_10044("10044", "alipay_order_cancle  :支付宝条码支付时，扣款失败，调用查询接口，订单状态为等待支付状态，调用撤销接口"),
    RESP_CODE_10045("10045", "alipay_cancle_fail / alipay_cancle_method_exception:调用支付宝撤销接口失败 /调用支付宝撤销接口异常"),
    RESP_CODE_10046("10046", "alipay_query_result_unknow / alipay_query_fail / alipay_query_exception:调用支付宝查询接口成功，响应未知状态 / 调用支付宝查询接口失败 / 调用支付宝查询接口异常"),
    RESP_CODE_10047("10047", "alipay_trade_pay_fail  :请求支付宝条码接口失败"),
    RESP_CODE_10048("10048", "alipay_trade_pay_exception:请求支付宝条码接口程序异常"),
    RESP_CODE_10049("10049", "alipay_create_response_is_null:支付宝固定二维码支付请求上游失败"),
    RESP_CODE_10050("10050", "alipay_trade_create_fail:支付宝固定二维码支付请求上游成功，预下单失败"),
    RESP_CODE_10051("10051", "alipay_trade_create_exception  :支付宝固定二维码支付请求上游程序异常"),
    RESP_CODE_10052("10052", "alipay_get_userid_fail  :支付宝获取userid失败"),
    RESP_CODE_10053("10053", "alipay_get_userid_exception  :支付宝获取userid程序异常"),
    RESP_CODE_10057("10057", "wechat_prepay_sign_fail  :微信预下单成功，签名验签失败"),
    RESP_CODE_10058("10058", "wechat_prepay_result_code_is_fail  :微信请求接口成功，预下单失败"),
    RESP_CODE_10059("10059", "wechat_prepay_return_code_is_fail  :微信请求接口返回失败"),
    RESP_CODE_10060("10060", "wechat_not_response_data  :微信请求接口无响应数据"),
    RESP_CODE_10061("10061", "wechat_unifiedorder_prepay_exception  :微信请求接口异常"),
    RESP_CODE_10062("10062", "wechat_reverse_result_code_is_fail  :微信撤销接口请求成功，撤销失败"),
    RESP_CODE_10063("10063", "wechat_reverse_return_code_is_fail  :微信撤销接口请求成功，撤销失败"),
    RESP_CODE_10064("10064", "wechat_reverse_not_response_data   :微信撤销接口请求无响应"),
    RESP_CODE_10065("10065", "wechat_reverse_method_exception  :微信撤销接口请求异常"),
    RESP_CODE_10066("10066", "退款请求，必须上传证书:退款请求，必须上传证书"),
    RESP_CODE_10067("10067", "微信退款接口请求异常!,请检验证书及信息是否有误！:微信退款接口请求异常!,请检验证书及信息是否有误！"),
    RESP_CODE_10068("10068", "微信退款失败:微信退款失败"),
    RESP_CODE_10069("10069", "微信退款签名失败:微信退款签名失败"),
    RESP_CODE_10070("10070", "微信退款请求失败:微信退款请求失败"),
    RESP_CODE_10071("10071", "微信退款请求无响应:微信退款请求无响应"),
    RESP_CODE_10072("10072", "微信退款请求异常:微信退款请求异常"),
    RESP_CODE_10073("10073", "wechat_query_result_code_is_fail  :微信查询接口请求成功，查询失败"),
    RESP_CODE_10074("10074", "wechat_query_return_code_is_fail  :微信查询接口请求成功，查询失败"),
    RESP_CODE_10075("10075", "wechat_query_not_response_data  :微信查询接口请求无响应"),
    RESP_CODE_10076("10076", "wechat_query_method_exception  :微信查询接口请求程序异常"),
    RESP_CODE_10077("10077", "request_api_exception  :商户请求api接口时程序异常"),
    RESP_CODE_10078("10078", "merchant_no_support_wechat_app:暂未开通微信固定二维码支付"),
    RESP_CODE_10079("10079", "amount_format_error  :支付宝固定二维码金额格式校验失败"),
    RESP_CODE_10080("10080", "merchant_get_aliJsUrl_exception:支付宝固定二维码拼接授权地址程序异常"),
    RESP_CODE_10081("10081", "ali_jspay_method_orderNo_is_null:支付宝获取openid方法未收到订单号"),
    RESP_CODE_10082("10082", "order_bean_is_null  :支付宝获取openid方法未找到对应订单信息"),
    RESP_CODE_10083("10083", "ali_auth_code_is_nRESP_CODE_ulL:支付宝获取openid方法未收到auth_code参数"),
    RESP_CODE_10084("10084", "merchant_not_configure_ali_parameter:未配置支付宝固定二维码参数"),
    RESP_CODE_10085("10085", "update_order_fail  :支付宝固定二维码下单成功后，更新订单信息失败"),
    RESP_CODE_10086("10086", "ali_get_openid_method_exception  :支付宝获取openid方法异常"),
    RESP_CODE_10087("10087", "amount_format_error:微信固定二维码金额格式校验失败"),
    RESP_CODE_10088("10088", "order_insert_fail  :微信固定二维码接口插入订单失败"),
    RESP_CODE_10089("10089", "merchant_rate_not_exist:微信固定二维码未配置费率"),
    RESP_CODE_10090("10090", "merchant_get_rate_type_fail:微信固定二维码未找到费率类型"),
    RESP_CODE_10091("10091", "merchant_not_configure_wechat_parameter:微信固定二维码未配置参数"),
    RESP_CODE_10092("10092", "merchant_not_configure_wechatJsPay_parameter:微信固定二维码未配置公众号参数"),
    RESP_CODE_10093("10093", "update_order_fail:微信固定二维码修改订单失败"),
    RESP_CODE_10094("10094", "merchant_get_wechatJsUrl_exception:拼接微信授权连接程序异常"),
    RESP_CODE_10095("10095", "wechat_fixqr_orderNo_is_null:微信获取openid方法未收到订单号"),
    RESP_CODE_10096("10096", "wechat_fixqr_order_not_exist:微信获取openid方法未找到订单信息"),
    RESP_CODE_10097("10097", "wechat_fixqr_code_is_null:微信获取openid方法未收到code参数"),
    RESP_CODE_10098("10098", "merchant_not_configure_wechat_parameter:微信获取openid未配置参数"),
    RESP_CODE_10099("10099", "wechat_fixqr_update_order_fail:微信获取openid成功，修改订单信息失败"),
    RESP_CODE_10100("10100", "wechat_fixqr_prepay_exception:微信获取openid方法程序异常"),
    RESP_CODE_10101("10101", "merchant_rate_not_exist:支付宝固定二维码未配置费率"),
    RESP_CODE_10102("10102", "merchant_get_rate_type_fail  :支付宝固定二维码费率类型未知"),
    RESP_CODE_10103("10103", "merchant_not_configure_parameter:支付宝固定二维码 未获取到配置参数"),
    RESP_CODE_10104("10104", "merchant_not_configure_aliJspay_parameter:支付宝固定二维码未配置js参数"),
    RESP_CODE_10105("10105", "update_order_fail:支付宝固定二维码修改订单失败"),
    RESP_CODE_10106("10106", "merchant_no_risk:商户未配置风控信息"),
    RESP_CODE_10107("10107", "merchant_trigger_single_max_amount_risk:商户触发单笔最大金额风控"),
    RESP_CODE_10108("10108", "merchant_trigger_single_min_amount_risk:商户触发单笔最小金额风控"),
    RESP_CODE_10109("10109", "merchant_trigger_day_total_amount_risk  :商户触发日总金额风控"),
    RESP_CODE_10110("10110", "merchant_trigger_day_total_count_risk   :商户触发日总下单笔数风控"),
    RESP_CODE_10111("10111", "merchant_trigger_trade_time_risk   :商户触发交易时间风控"),
    RESP_CODE_10112("10112", "merchant_execute_risk_method_exception  :执行风控方法程序异常"),
    RESP_CODE_10113("10113", "merchant_no_support_this_app:暂不支持该app扫码"),
    RESP_CODE_10114("10114", "power_not_configure_fee_rate  :该功能未配置上游费率"),
    RESP_CODE_10115("10115", "wechat_h5_sceneInfo_is_invalid  :微信h5接口，商户所传sceneInfo字段格式不对"),
    RESP_CODE_10116("10116", "authCode_is_nuRESP_CODE_ll商户下单时，当payPowerId所传值为2、 5、 9时，必须传authCode字段"),
    RESP_CODE_10117("10117", "authCode_length_is_too_long  :authCode传值太长，最大长度64位"),
    RESP_CODE_10118("10118", "token_is_null:二维码无效，token值为空"),
    RESP_CODE_10119("10119", "token_is_invalid:二维码无效，token值无效"),
    RESP_CODE_10120("10120", "merchant_token_status_is_invalid:token值对应状态无法交易"),
    RESP_CODE_10121("10121", "token_not_found_merchat:该二维码未找到商家信息"),
    RESP_CODE_10122("10122", "merchant_status_is_invalid:商户状态暂时不支持支付"),
    RESP_CODE_10123("10123", "merchant_no_configure_relation:商户未配置支付通道"),
    RESP_CODE_10124("10124", "qr_method_exception:固定二维码接口异常"),
    RESP_CODE_10125("10125", "merchant_token_type_is_invalid:token值类型不支持固定二维码"),
    RESP_CODE_10126("10126", "terminalId_is_null:条码支付时，未传设备号参数"),
    RESP_CODE_10127("10127", "terminalId_length_is_too_long:terminalId字段值超过32位"),
    RESP_CODE_10128("10128", "qq_prepay_sign_fail:qq支付预下单结果验签失败"),
    RESP_CODE_10129("10129", "qq_prepay_result_code_is_fail:qq支付预下单失败"),
    RESP_CODE_10130("10130", "qq_prepay_return_code_is_fail:qq支付预下单失败"),
    RESP_CODE_10131("10131", "qq_not_response_data:qq支付预下单无响应"),
    RESP_CODE_10132("10132", "qq_unifiedorder_prepay_exception:qq支付预下单异常"),
    RESP_CODE_10133("10133", "qq_prepay_result_code_is_fail_**:qq条码支付预下单失败"),
    RESP_CODE_10134("10134", "qq_prepay_return_code_is_fail:qq条码支付预下单失败"),
    RESP_CODE_10135("10135", "refundType_is_null:退款类型没有填写"),
    RESP_CODE_10136("10136", "refundType_is_noExists:退款类型不存在"),
    RESP_CODE_10137("10137", "RefundFee_is_null:退款费用没有填写"),
    RESP_CODE_10138("10138", "refOrder_is_noExists:退款订单不存在"),
    RESP_CODE_10139("10139", "order_is_noExists:订单不存在"),
    RESP_CODE_10140("10140", "order_pay_error:订单未支付成功"),
    RESP_CODE_10141("10141", "qq_reverse_result_code_is_fail:qq撤销接口失败"),
    RESP_CODE_10142("10142", "qq_reverse_return_code_is_fail:qq撤销接口失败"),
    RESP_CODE_10143("10143", "qq_reverse_not_response_data:qq撤销接口无响应"),
    RESP_CODE_10144("10144", "qq_reverse_method_exception:qq撤销接口异常"),
    RESP_CODE_10145("10145", "qq_prepay_sign_fail:qq条码支付结果验签失败"),
    RESP_CODE_10146("10146", "amount_format_error:金额格式不对"),
    RESP_CODE_10147("10147", "order_insert_fail:qq固定码支付生成订单失败"),
    RESP_CODE_10148("10148", "merchant_rate_not_exist:商户参数未配置完整"),
    RESP_CODE_10149("10149", "merchant_get_rate_type_fail:商户参数未配置完整"),
    RESP_CODE_10150("10150", "qqjs_power_not_configure_fee_rate:商户支付通道未配置完整"),
    RESP_CODE_10151("10151", "merchant_not_configure_qq_parameter:商户参数未配置完整"),
    RESP_CODE_10152("10152", "qq_fixqr_update_order_fail:修改订单状态失败"),
    RESP_CODE_10153("10153", "qq_fixqr_prepay_exception:qq固定二维支付请求异常"),
    RESP_CODE_10154("10154", "qq_not_response_data:qq条码预下单未响应结果"),
    RESP_CODE_10155("10155", "qq_micropay_prepay_exception:qq条码支付预下单异常"),
    RESP_CODE_10156("10156", "qq_query_result_code_is_fail:qq查询接口失败"),
    RESP_CODE_10157("10157", "qq_query_return_code_is_fail:qq查询接口失败"),
    RESP_CODE_10158("10158", "qq_query_not_response_data:qq查询接口无响应"),
    RESP_CODE_10159("10159", "qq_query_method_exception:qq查询接口异常"),
    RESP_CODE_10160("10160", "qq_order_already_reverse:qq条码支付超时，已自动撤销"),
    RESP_CODE_10161("10161", "wechat_order_already_reverse:微信条码支付超时，已自动撤销"),
    RESP_CODE_10162("10162", "order_mchntId_not_exist:订单没有商户id"),
    RESP_CODE_10163("10163", "mchntId_agentId_not_exist:该商户有代理商关系但没有找到该代理商"),
    RESP_CODE_10164("10164", "mchntId_agentId_relation_multiple:商户代理商关系有多条"),
    RESP_CODE_10165("10165", "agentId_rate_not_exist:代理商费率不存在"),
    RESP_CODE_10166("10166", "agentId_order_fee_error:该商户的代理商费用更新数据库失败"),
    RESP_CODE_10167("10167", "orderAmt_refundAmt_different:全额退款金额与订单交易金额不一致"),
    RESP_CODE_10168("10168", "order_refunded:该笔订单已全额退款完毕"),
    RESP_CODE_10169("10169", "refundAmount_exceeded_balance:退款金额无效,已超出订单余额"),
    RESP_CODE_10170("10170", "mchntOrderNo_repeat:商户订单号重复，每个订单号只能预下单一次"),
    RESP_CODE_10171("10171", "支付宝退款接口调用失败:支付宝退款接口调用失败"),
    RESP_CODE_10172("10172", "支付宝退款失败:支付宝退款失败"),
    RESP_CODE_10173("10173", "支付宝退款接口调用异常:支付宝退款接口调用异常"),
    RESP_CODE_10174("10174", "微信退款处理中:微信退款处理中"),
    RESP_CODE_10175("10175", "微信退款关闭:微信退款关闭"),
    RESP_CODE_10176("10176", "微信退款失败:微信退款失败"),
    RESP_CODE_10177("10177", "微信退款查询签名失败:微信退款查询签名失败"),
    RESP_CODE_10178("10178", "微信退款查询请求失败:微信退款查询请求失败"),
    RESP_CODE_10179("10179", "微信退款查询请求无响应:微信退款查询请求无响应"),
    RESP_CODE_10180("10180", "微信退款查询请求异常:微信退款查询请求异常"),
    RESP_CODE_10181("10181", "wechat_get_openid_exception:微信获取openid异常"),
    RESP_CODE_10182("10182", "京东扫码预下单失败:京东扫码预下单失败"),
    RESP_CODE_10183("10183", "京东扫码接口调用异常:京东扫码接口调用异常"),
    RESP_CODE_10184("10184", "京东支付预下单失败:京东支付预下单失败"),
    RESP_CODE_10185("10185", "京东支付接口调用异常:京东支付接口调用异常"),
    RESP_CODE_10186("10186", "京东条码支付失败:京东条码支付失败"),
    RESP_CODE_10187("10187", "京东条码支付发生异常:京东条码支付发生异常"),
    RESP_CODE_10188("10188", "merchant_no_support_jd_app:商户不支持京东app"),
    RESP_CODE_10189("10189", "amount_format_error:金额格式不对"),
    RESP_CODE_10190("10190", "order_insert_fail:订单生成失败"),
    RESP_CODE_10191("10191", "merchant_rate_not_exist:商户费率未配置"),
    RESP_CODE_10192("10192", "merchant_get_rate_type_fail:商户费率类型获取失败"),
    RESP_CODE_10193("10193", "jdjs_power_not_configure_fee_rate:京东固定二维码未配置费率"),
    RESP_CODE_10194("10194", "merchant_not_configure_parameter:商户未配置完整参数"),
    RESP_CODE_10195("10195", "update_order_fail:修改订单失败"),
    RESP_CODE_10196("10196", "merchant_get_jdstr_exception:获取京东参数异常"),
    RESP_CODE_10197("10197", "京东支付fix预下单失败:京东支付fix预下单失败"),
    RESP_CODE_10198("10198", "京东支付fix接口调用异常:京东支付fix接口调用异常"),
    RESP_CODE_10199("10199", "unionpay_wechat_micropay_prepay_exception:银联微信条码支付下单异常"),
    RESP_CODE_10200("10200", "unionpay_wechat_not_response_data:银联微信条码请求上游无响应"),
    RESP_CODE_10201("10201", "参考银联返回具体信息:银联条码下单通讯失败"),
    RESP_CODE_10202("10202", "参考银联返回具体信息:银联条码下单业务失败"),
    RESP_CODE_10203("10203", "unionpay_wechat_prepay_sign_fail:银联微信预下单失败"),
    RESP_CODE_10204("10204", "unionpay_wechat_query_method_exception:银联微信执行查询接口异常"),
    RESP_CODE_10205("10205", "unionpay_wechat_query_not_response_data:银联微信请求上游查询接口无响应"),
    RESP_CODE_10206("10206", "参考银联返回具体信息:银联查询通讯失败"),
    RESP_CODE_10207("10207", "参考银联返回具体信息:银联查询业务失败"),
    RESP_CODE_10208("10208", "unionpay_wechat_reverse_method_exception:银联微信执行撤销接口异常"),
    RESP_CODE_10209("10209", "unionpay_wechat_reverse_not_response_data:银联微信撤单请求上游无响应"),
    RESP_CODE_10210("10210", "参考银联返回具体信息:银联微信撤销通讯失败"),
    RESP_CODE_10211("10211", "参考银联返回具体信息:银联微信撤销业务失败"),
    RESP_CODE_10212("10212", "unionpay_wechat_unifiedorder_prepay_exception:银联微信支付统一下单异常"),
    RESP_CODE_10213("10213", "unionpay_wechat_not_response_data:银联微信统一下单请求上游无响应"),
    RESP_CODE_10214("10214", "参考银联返回具体信息:银联微信统一下单通讯失败"),
    RESP_CODE_10215("10215", "参考银联返回具体信息:银联微信统一下单业务失败"),
    RESP_CODE_10216("10216", "unionpay_wechat_prepay_sign_fail:银联微信统一下单验签失败"),
    RESP_CODE_10217("10217", "unionpay_wechat_micropay_close_exception:银联微信关单异常"),
    RESP_CODE_10218("10218", "unionpay_wechat_reverse_not_response_data:银联微信关单请求上游无响应"),
    RESP_CODE_10219("10219", "参考银联返回具体信息:银联微信关闭订单通讯失败"),
    RESP_CODE_10220("10220", "参考银联返回具体信息:银联微信关闭订单业务失败"),
    RESP_CODE_10221("10221", "unionpay_wechat_order_closed:银联微信的订单已经被关闭"),
    RESP_CODE_10222("10222", "uinonpay_wechat_order_already_reverse:银联微信的订单已经被撤销"),
    RESP_CODE_10223("10223", "unionpay_amount_format_error:银联微信固定二维码金额格式校验失败"),
    RESP_CODE_10224("10224", "unionpay_order_insert_fail:银联微信固定二维码接口插入订单失败"),
    RESP_CODE_10025("10025", "unionpay_merchant_rate_not_exist:银联微信固定二维码未配置费率"),
    RESP_CODE_10226("10226", "unionpay_merchant_get_rate_type_fail:银联微信固定二维码未找到费率类型"),
    RESP_CODE_10227("10227", "unionpay_wxjs_power_not_configure_fee_rate:银联微信固定二维码为配置上游费率"),
    RESP_CODE_10228("10228", "unionpay_merchant_not_configure_wechat_parameter:银联微信固定二维码未配置参数"),
    RESP_CODE_10229("10229", "unionpay_merchant_not_configure_wechatJsPay_parameter:银联微信固定二维码未配置公众号参数"),
    RESP_CODE_10230("10230", "unionpay_update_order_fail:银联微信固定二维码修改订单失败"),
    RESP_CODE_10231("10231", "unionpay_merchant_get_wechatJsUrl_exception:银联微信固定二维码拼接微信授权连接程序异常"),
    RESP_CODE_10232("10232", "unionpay_wechat_fixqr_orderNo_is_null:银联微信获取openid方法未收到订单号"),
    RESP_CODE_10233("10233", "unionpay_wechat_fixqr_order_not_exist:银联微信获取openid方法未找到订单信息"),
    RESP_CODE_10234("10234", "unionpay_wechat_fixqr_code_is_null:银联微信获取openid方法未收到code参数"),
    RESP_CODE_10235("10235", "unionpay_merchant_not_configure_wechat_parameter:银联微信获取openid未配置参数"),
    RESP_CODE_10236("10236", "unionpay_wechat_fixqr_update_order_fail:银联微信获取openid成功，修改订单信息失败"),
    RESP_CODE_10237("10237", "unionpay_wechat_fixqr_prepay_exception:银联微信获取openid方法程序异常"),
    RESP_CODE_10238("10238", "unionpay_merchant_no_support_wechat_app:暂未开通银联微信固定二维码支付"),
    RESP_CODE_10239("10239", "wait_user_pay:用户支付中"),
    RESP_CODE_20007("20007", "mchntId_is_invalid:所传商户编号值无效"),
    RESP_CODE_20008("20008", "check_sign_fail:验签失败"),
    RESP_CODE_20009("20009", "check_sign_exception:验签异常，联系支付平台技术排查"),
    RESP_CODE_20010("20010", "request_parameter_is_null:请求参数不能为空（rsa密文）"),
    RESP_CODE_20011("20011", "doTask_method_exception:业务处理异常，联系支付平台技术排查"),
    RESP_CODE_20012("20012", "order_do_not_exist:查询订单不存在"),
    RESP_CODE_20013("20013", "order_nonuniqueness:订单不唯一"),
    RESP_CODE_20014("20014", "file_do_not_exist:对账文件未生成"),
    RESP_CODE_20015("20015", "file_nonuniqueness:对账文件不唯一"),
    RESP_CODE_20016("20016", "downfile_parameter_lack:下载对账文件接口参数不全"),
    RESP_CODE_20017("20017", "file_path_is_null:对账文件未生成"),
    RESP_CODE_20018("20018", "downfile_method_exception:下载对账文件异常"),
    RESP_CODE_20019("20019", "authUrl_not_found_orderNo:授权链接上未发现订单号"),
    RESP_CODE_20020("20020", "authUrl_order_bean_is_null:获取授权时找不到订单"),
    RESP_CODE_20021("20021", "authUrl_order_is_not_jspay:该笔订单不是固定二维码支付"),
    RESP_CODE_20022("20022", "authUrl_not_configure_ali_parameter:商户参数未配置完整"),
    RESP_CODE_20023("20023", "authUrl_not_configure_aliJspay_parameter:商户参数未配置完整"),
    RESP_CODE_20024("20024", "authUrl_method_exception:获取授权异常"),
    RESP_CODE_20025("20025", "returnMerchat_method_exception:同步跳转商户returnurl时异常"),
    RESP_CODE_20026("20026", "returnMerchat_not_found_orderNo:同步跳转商户找不到订单号"),
    RESP_CODE_20027("20027", "returnMerchat_order_bean_is_null:同步跳转时订单号无效"),
    RESP_CODE_20028("20028", "qq退款请求，没有读取到证书路径:qq退款请求，没有读取到证书路径"),
    RESP_CODE_20029("20029", "qq退款接口请求异常!,请检验证书及信息是否有误:qq退款接口请求异常!,请检验证书及信息是否有误"),
    RESP_CODE_20030("20030", "qq退款请求失败，失败原因:{详见qq返回}:qq退款请求失败，失败原因:{详见qq返回}"),
    RESP_CODE_20031("20031", "qq退款验签失败:qq退款验签失败"),
    RESP_CODE_20032("20032", "qq退款请求无响应:qq退款请求无响应"),
    RESP_CODE_20033("20033", "qq退款请求异常:qq退款请求异常"),
    RESP_CODE_20034("20034", "the balance of the day is less than the refund amount:当日交易余额小于本次退款金额"),
    RESP_CODE_20035("20035", "以上有返回结果为准:以上有返回结果为准（结果失败）"),
    RESP_CODE_20036("20036", "以上有返回结果为准:以上有返回结果为准（请求失败）"),
    RESP_CODE_20037("20037", "unionpay_wechat_refund_fail:银联微信退款请求异常"),
    RESP_CODE_20038("20038", "unionpay_wechat_refund_query_single_fail:银联微信单笔退款查询请求异常"),
    RESP_CODE_20039("20039", "unionpay_wechat_refund_query_multiple_fail:银联微信多笔退款查询请求异常"),
    RESP_CODE_20040("20040", "unionpay_wechat_refunds_change_fail:银联微信多笔退款查询结果转换异常"),
    RESP_CODE_20041("20041", "unionpay_wechat_refunds_refund_fee_change_fail:银联微信多笔退款查询结果退款金额转换异常"),
    RESP_CODE_20042("20042", "unionpay_wechat_refunds_update_fail:银联微信多笔退款查询结果更新异常"),
    RESP_CODE_20043("20043", "unionpay_wechat_refund_result_fail:银联微信退款结果失败"),
    RESP_CODE_20044("20044", "unionpay_wechat_refund_result_refunding:银联微信退款结果进行中"),
    RESP_CODE_20045("20045", "wechat_close_result_code_is_fail:微信关单请求成功, 关单失败"),
    RESP_CODE_20046("20046", "wechat_close_return_code_is_fail:微信请求关单失败"),
    RESP_CODE_20047("20047", "wechat_close_not_response_data:微信关单请求上游无响应"),
    RESP_CODE_20048("20048", "wechat_close_method_exception:微信执行关单接口异常"),
    RESP_CODE_30001("30001", "请求富民交易接口失败:请求富民交易接口失败"),
    RESP_CODE_30002("30002", "请求富民交易系统异常:请求富民交易系统异常"),


    ;

    private final String code;
    private final String message;

    private FbankRespCodeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (FbankRespCodeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static FbankRespCodeEnum find(String code) {
        for (FbankRespCodeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<FbankRespCodeEnum> getAll() {
        List<FbankRespCodeEnum> list = new ArrayList<FbankRespCodeEnum>();
        for (FbankRespCodeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (FbankRespCodeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }
}
