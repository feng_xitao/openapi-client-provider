package com.acooly.module.openapi.client.provider.fbank.signature;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.provider.fbank.FbankConstants;
import com.acooly.module.safety.exception.SignatureVerifyException;
import com.acooly.module.safety.signature.Signer;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author zhike 2017/11/28 11:41
 */
@Component("safetyFbankMd5Signer")
public class FbankMd5Signer implements Signer<Map<String, String>, String> {

    @Override
    public String sign(Map<String, String> plain, String key) {
        String waitToSignStr = getWaitToSigin(plain);
        // MD5摘要签名计算
        return DigestUtils.md5Hex(waitToSignStr + "&key=" + key);
    }

    @Override
    public void verify(Map<String, String> plain, String key, String verifySign) {
        String waitToSignStr = getWaitToSigin(plain);
        // MD5摘要签名计算
        String signature = DigestUtils.md5Hex(waitToSignStr + "&key=" + key);
        if (!verifySign.equalsIgnoreCase(signature)) {
            throw new SignatureVerifyException("验签未通过");
        }
    }

    protected String getWaitToSigin(Map<String, String> plain) {
        String waitToSignStr = null;
        Map<String, String> sortedMap = new TreeMap(plain);
        if (sortedMap.containsKey(FbankConstants.SIGN_KEY)) {
            sortedMap.remove(FbankConstants.SIGN_KEY);
        }

        StringBuilder stringToSign = new StringBuilder();
        if (sortedMap.size() > 0) {
            Iterator var5 = sortedMap.entrySet().iterator();

            while (var5.hasNext()) {
                Map.Entry<String, String> entry = (Map.Entry) var5.next();
                if (Strings.isNotBlank(entry.getValue())) {
                    stringToSign.append((String) entry.getKey()).append("=").append((String) entry.getValue()).append("&");
                }
            }

            stringToSign.deleteCharAt(stringToSign.length() - 1);
            waitToSignStr = stringToSign.toString();
        }

        return waitToSignStr;
    }

    @Override
    public String getSinType() {
        return FbankConstants.SIGNER_TYPE;
    }
}




