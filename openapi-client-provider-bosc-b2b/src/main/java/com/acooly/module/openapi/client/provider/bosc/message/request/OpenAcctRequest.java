package com.acooly.module.openapi.client.provider.bosc.message.request;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

import com.acooly.module.openapi.client.provider.bosc.annotation.BoscAlias;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequestDomain;
import com.acooly.module.openapi.client.provider.bosc.enums.OpenTypeEnum;

@Getter
@Setter
public class OpenAcctRequest extends BoscRequestDomain {

	@BoscAlias(isSign = false)
	private OpenTypeEnum openType;

	/**
	 * 中文客户全称（中文企业全称）
	 */
	@NotBlank
	private String cNName;

	/**
	 * 证件号码
	 */
	@NotBlank
	private String idNo;

	/**
	 * 法人代表姓名
	 */
	@NotBlank
	private String corpName;

	/**
	 * 法人代表人证件号
	 */
	@NotBlank
	private String legPerId;

	/**
	 * 法人代表联系电话
	 */
	@NotBlank
	private String legalPhone;

	/**
	 * 绑定卡清算行行号
	 */
	@NotBlank
	private String acctBank;

	/**
	 * 绑定银行对公账号
	 */
	@NotBlank
	private String account;

	/**
	 * 手机动态验证码
	 */
	@NotBlank
	private String dynamicCode;

	/**
	 * 英文客户全称（英文企业全称）
	 */
	private String gBName;

	/**
	 * 制单员姓名
	 */
	private String docuOpName;

	/**
	 * 制单员手机号
	 */
	private String docuOpMobile;

	/**
	 * 制单员身份证号码
	 */
	private String docuOpIdCard;

	/**
	 * 复核员姓名
	 */
	private String checkerName;

	/**
	 * 复核员手机号
	 */
	private String checkerMobile;

	/**
	 * 复核员身份证号码
	 */
	private String checkerIdCard;

}
