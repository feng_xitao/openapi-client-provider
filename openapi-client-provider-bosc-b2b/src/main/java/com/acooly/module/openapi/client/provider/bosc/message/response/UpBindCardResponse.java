package com.acooly.module.openapi.client.provider.bosc.message.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponseDomain;

@Getter
@Setter
public class UpBindCardResponse extends BoscResponseDomain {

	/**
	 * 打款金额
	 */
	private Money amount;

	/**
	 * 打款激活最后日期
	 */
	private Date actiDeadline;

}
