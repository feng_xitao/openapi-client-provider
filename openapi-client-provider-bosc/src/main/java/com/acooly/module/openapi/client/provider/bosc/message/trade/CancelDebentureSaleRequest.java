package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.CANCEL_DEBENTURE_SALE, type = ApiMessageType.Request)
public class CancelDebentureSaleRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 债权出让请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String creditsaleRequestNo;
	
	public CancelDebentureSaleRequest () {
		setService (BoscServiceNameEnum.CANCEL_DEBENTURE_SALE.code ());
	}
	
	public CancelDebentureSaleRequest (String requestNo, String creditsaleRequestNo) {
		this();
		this.requestNo = requestNo;
		this.creditsaleRequestNo = creditsaleRequestNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getCreditsaleRequestNo () {
		return creditsaleRequestNo;
	}
	
	public void setCreditsaleRequestNo (String creditsaleRequestNo) {
		this.creditsaleRequestNo = creditsaleRequestNo;
	}
}