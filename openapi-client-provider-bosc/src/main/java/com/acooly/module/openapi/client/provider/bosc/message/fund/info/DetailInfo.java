package com.acooly.module.openapi.client.provider.bosc.message.fund.info;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.enums.trade.BoscBizTypeEnum;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.PrimitiveIterator;

/**
 * Created by liubin@prosysoft.com on 2017/10/10.
 */
public class DetailInfo {
	
	@NotNull(message = "业务类型不能为空")
	private BoscBizTypeEnum bizType;
	
	/**
	 * 预处理请求流水号
	 */
	@Max (value = 50)
	private String freezeRequestNo;
	
	/**
	 * 出款方用户编号
	 */
	@Max (value = 50)
	private String sourcePlatformUserNo;
	
	/**
	 * 收款方用户编号
	 */
	@Max (value = 50)
	private String targetPlatformUserNo;
	
	/**
	 * 交易金额（有利息时为本息）
	 */
	@MoneyConstraint(min = 1,message = "交易金额不能为空",nullable = false)
	private Money amount;
	
	/**
	 * 利息
	 */
	private Money income;
	
	/**
	 * 债权份额（债权认购且需校验债权关系的必传）
	 */
	private Money share;
	
	public BoscBizTypeEnum getBizType () {
		return bizType;
	}
	
	public void setBizType (BoscBizTypeEnum bizType) {
		this.bizType = bizType;
	}
	
	public String getFreezeRequestNo () {
		return freezeRequestNo;
	}
	
	public void setFreezeRequestNo (String freezeRequestNo) {
		this.freezeRequestNo = freezeRequestNo;
	}
	
	public String getSourcePlatformUserNo () {
		return sourcePlatformUserNo;
	}
	
	public void setSourcePlatformUserNo (String sourcePlatformUserNo) {
		this.sourcePlatformUserNo = sourcePlatformUserNo;
	}
	
	public String getTargetPlatformUserNo () {
		return targetPlatformUserNo;
	}
	
	public void setTargetPlatformUserNo (String targetPlatformUserNo) {
		this.targetPlatformUserNo = targetPlatformUserNo;
	}
	
	public Money getAmount () {
		return amount;
	}
	
	public void setAmount (Money amount) {
		this.amount = amount;
	}
	
	public Money getIncome () {
		return income;
	}
	
	public void setIncome (Money income) {
		this.income = income;
	}
	
	public Money getShare () {
		return share;
	}
	
	public void setShare (Money share) {
		this.share = share;
	}
	
	@Override
	public String toString () {
		return new ToStringBuilder (this)
				.toString ();
	}
}
