package com.acooly.module.openapi.client.provider.bosc.message.member;

import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscNotify;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.fund.BoscBankcodeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscAccessTypeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscAuditStatusEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscCardNolsChangeEnum;
import com.acooly.module.openapi.client.provider.bosc.enums.member.BoscUserRoleEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.ACTIVATE_STOCKED_USER, type = ApiMessageType.Notify)
public class ActivateStockedUserNotify extends BoscNotify {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 银行卡号或企业对公账户
	 */
	@NotEmpty
	@Size(max = 50)
	private String bankcardNo;
	/**
	 * 见【银行编码】
	 */
	@NotNull
	private BoscBankcodeEnum bankcode;
	/**
	 * 手机号
	 */
	@NotEmpty
	@Size(max = 50)
	private String mobile;
	/**
	 * 见【鉴权通过类型】
	 */
	@NotNull
	private BoscAccessTypeEnum accessType;
	/**
	 * 见【用户角色】
	 */
	@NotNull
	private BoscUserRoleEnum userRole;
	/**
	 * 见【审核状态】，该接口会发送多次不同审核状态的回调
	 */
	@NotNull
	private BoscAuditStatusEnum auditStatus;
	/**
	 * 主动换卡标识；TRUE 表示个人用户在页面上修改过已经导入的卡号，FALSE 表示 个人用户是主动填写或没有在页面上修改过卡号；企业用户激活时不可修改对公账 号故不返回
	 */
	private BoscCardNolsChangeEnum cardNolsChange;
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getBankcardNo () {
		return bankcardNo;
	}
	
	public void setBankcardNo (String bankcardNo) {
		this.bankcardNo = bankcardNo;
	}
	
	public BoscBankcodeEnum getBankcode () {
		return bankcode;
	}
	
	public void setBankcode (BoscBankcodeEnum bankcode) {
		this.bankcode = bankcode;
	}
	
	public String getMobile () {
		return mobile;
	}
	
	public void setMobile (String mobile) {
		this.mobile = mobile;
	}
	
	public BoscAccessTypeEnum getAccessType () {
		return accessType;
	}
	
	public void setAccessType (BoscAccessTypeEnum accessType) {
		this.accessType = accessType;
	}
	
	public BoscUserRoleEnum getUserRole () {
		return userRole;
	}
	
	public void setUserRole (BoscUserRoleEnum userRole) {
		this.userRole = userRole;
	}
	
	public BoscAuditStatusEnum getAuditStatus () {
		return auditStatus;
	}
	
	public void setAuditStatus (BoscAuditStatusEnum auditStatus) {
		this.auditStatus = auditStatus;
	}
	
	public BoscCardNolsChangeEnum getCardNolsChange () {
		return cardNolsChange;
	}
	
	public void setCardNolsChange (
			BoscCardNolsChangeEnum cardNolsChange) {
		this.cardNolsChange = cardNolsChange;
	}
}