package com.acooly.module.openapi.client.provider.allinpay.marshall;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.message.PostRedirect;
import com.acooly.module.openapi.client.provider.allinpay.domain.AllinpayRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;


@Service
@Slf4j
public class AllinpayRedirectPostMarshall extends AllinpayMarshallSupport implements ApiMarshal<PostRedirect, AllinpayRequest> {

    @Override
    public PostRedirect marshal(AllinpayRequest source) {
        PostRedirect postRedirect = new PostRedirect();
        if(Strings.isBlank(source.getGatewayUrl())) {
            source.setGatewayUrl(getProperties().getGatewayUrl());
        }
        postRedirect.setRedirectUrl(source.getGatewayUrl());
        log.info("跳转报文: {}", postRedirect);
        return postRedirect;
    }
}
