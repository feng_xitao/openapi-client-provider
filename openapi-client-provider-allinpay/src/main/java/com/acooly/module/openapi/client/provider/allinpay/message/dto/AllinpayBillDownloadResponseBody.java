package com.acooly.module.openapi.client.provider.allinpay.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @Auther: zhike
 * @Date: 2018/8/29 19:41
 * @Description:
 */
@Getter
@Setter
@XStreamAlias("DOWNRSP")
public class AllinpayBillDownloadResponseBody implements Serializable {
    /**
     * 返回码
     */
    @Size(max = 4)
    @NotBlank
    @XStreamAlias("RET_CODE")
    private String retCode;

    /**
     * 内容
     * 每天一个对账文件，所有查询到的的对账文件使用ZIP格式压缩，然后使用BASE64编码
     */
    @XStreamAlias("CONTENT")
    private String content;
}
