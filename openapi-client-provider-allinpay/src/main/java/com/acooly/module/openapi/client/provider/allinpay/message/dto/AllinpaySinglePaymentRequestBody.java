package com.acooly.module.openapi.client.provider.allinpay.message.dto;

import com.acooly.core.utils.Dates;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @Auther: zhike
 * @Date: 2018/8/29 19:12
 * @Description:
 */
@Getter
@Setter
@ToString
@XStreamAlias("TRANS")
public class AllinpaySinglePaymentRequestBody implements Serializable {

    /**
     * 订单号(传入，组件自动组装到head里面去)
     */
    @XStreamOmitField
    @NotBlank
    private String bizOrderNo;

    /**
     * 业务代码
     */
    @Size(max = 6)
    @NotBlank
    @XStreamAlias("BUSINESS_CODE")
    private String businessCode;

    /**
     * 商户代码（通联通装莽，调用的时候不用传）
     */
    @Size(max = 15)
    @NotBlank
    @XStreamAlias("MERCHANT_ID")
    private String merchantId;

    /**
     *
     */
    @Size(max = 14)
    @NotBlank
    @XStreamAlias("SUBMIT_TIME")
    private String submitTime = Dates.format(new Date(),"yyyyMMddHHmmss");

    /**
     * 用户编号
     * 客户编号,开发人员可当作备注字段
     */
    @Size(max = 20)
    @XStreamAlias("E_USER_CODE")
    private String eUserCode;

    /**
     * 效期
     * MMYY，用于信用卡
     * 根据业务需要
     */
    @Size(max = 8)
    @XStreamAlias("VALIDATE")
    private String validate;

    /**
     * 信用卡CVV2
     * 仅用于信用卡
     */
    @Size(max = 3)
    @NotBlank
    @XStreamAlias("CVV2")
    private String cvv;

    /**
     * 银行代码
     * 银行代码，存折必须填写。参见附录A.3银行代码
     * 不填根据卡bin决定
     * com.acooly.module.openapi.client.provider.allinpay.enums.AllinpayBankCodeEnum
     */
    @Size(max = 8)
    @XStreamAlias("BANK_CODE")
    private String bankCode;

    /**
     * 开户行名称
     * 开户行详细名称，也叫网点，如 中国建设银行广州东山广场分理处。
     */
    @Size(max = 60)
    @XStreamAlias("BANK_NAME")
    private String bankName;

    /**
     * 账号类型
     * 00银行卡，01存折，02信用卡。不填默认为银行卡00。
     */
    @Size(max = 2)
    @XStreamAlias("ACCOUNT_TYPE")
    private String accountType;

    /**
     * 账号
     * 银行卡或存折号码
     */
    @Size(max = 32)
    @NotBlank
    @XStreamAlias("ACCOUNT_NO")
    private String accountNo;

    /**
     * 账号名
     * 银行卡或存折上的所有人姓名。
     */
    @Size(max = 15)
    @NotBlank
    @XStreamAlias("ACCOUNT_NAME")
    private String accountName;

    /**
     * 开户行所在省
     * 不带“省”或“自治区”，如 广东，广西，内蒙古等。
     * 建议根据后附的中国邮政区号表内的“省洲名称”列的内容填写。
     * 看具体渠道，有些必须上送，联系客户经理
     */
    @Size(max = 20)
    @XStreamAlias("PROVINCE")
    private String province;

    /**
     * 开户行所在市
     * 不带“市”，如 广州，南宁等。
     * 如果是直辖市，则填区，如北京（市）朝阳（区）。
     * 建议根据后附的中国邮政区号表内的“地区名称”列的内容填写。
     * 看具体渠道，有些必须上送，联系客户经理
     */
    @Size(max = 20)
    @XStreamAlias("CITY")
    private String city;

    /**
     * 账号属性
     * 0私人，1公司。不填时，默认为私人0。
     */
    @Size(max = 1)
    @NotBlank
    @XStreamAlias("ACCOUNT_PROP")
    private String accountProp = "0";

    /**
     * 金额
     * 整数，单位分
     */
    @Size(max = 12)
    @NotBlank
    @XStreamAlias("AMOUNT")
    private String amount;

    /**
     * 货币类型
     * 人民币：CNY, 港元：HKD，美元：USD。不填时，默认为人民币。
     */
    @Size(max = 3)
    @XStreamAlias("CURRENCY")
    private String currency = "CNY";

    /**
     * 开户证件类型
     * com.acooly.module.openapi.client.provider.allinpay.enums.AllinpayIdTypeEnum
     */
    @Size(max = 1)
    @NotBlank
    @XStreamAlias("ID_TYPE")
    private String idType;

    /**
     * 证件号
     */
    @Size(max = 22)
    @XStreamAlias("ID")
    private String idCardNo;

    /**
     * 本交易结算户
     * 结算到商户的账户，不需分别清算时不需填写。
     */
    @Size(max = 32)
    @XStreamAlias("SETTACCT")
    private String settAcct;

    /**
     * 手机号/小灵通
     * 小灵通带区号，不带括号，减号
     */
    @Size(max = 13)
    @XStreamAlias("TEL")
    private String mobileNo;

    /**
     * 自定义用户号
     * 商户自定义的用户号，开发人员可当作备注字段使用
     */
    @Size(max = 128)
    @XStreamAlias("CUST_USERID")
    private String custUserId;

    /**
     * 分组清算标志
     * 特殊商户使用，普通商户不要填
     */
    @Size(max = 30)
    @XStreamAlias("SETTGROUPFLAG")
    private String settGroupFlag;

    /**
     * 支付行号
     */
    @Size(max = 12)
    @XStreamAlias("UNION_BANK")
    private String unionBank;

    /**
     * 交易附言
     * 填入网银的交易备注
     */
    @Size(max = 30)
    @XStreamAlias("SUMMARY")
    private String summary;

    /**
     * 备注
     * 供商户填入参考信息
     */
    @Size(max = 50)
    @XStreamAlias("REMARK")
    private String remark;
}
