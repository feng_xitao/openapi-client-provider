package com.acooly.openapi.client.provider.fbank;

import com.acooly.core.common.BootApp;
import com.acooly.core.utils.Ids;
import com.acooly.core.utils.system.IPUtil;
import com.acooly.module.openapi.client.provider.fbank.FbankApiService;
import com.acooly.module.openapi.client.provider.fbank.message.*;
import com.acooly.test.NoWebTestBase;
import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author zhike@qq.com
 * @date 2018-09-06 15:07
 */
@Slf4j
@SpringBootApplication
@BootApp(sysName = "test")
public class FbankTest extends NoWebTestBase {

    @Autowired
    private FbankApiService fbankApiService;

    /**
     * 聚合支付测试(主扫)
     */
    @Test
    public void testPrepay51() {
        FbankPrepayRequest request = new FbankPrepayRequest();
        request.setMchntOrderNo(Ids.oid());
        request.setPayPowerId("51");//42：公众号，50：被扫，51主扫
        request.setAmount("1");
        request.setClientIp(IPUtil.getFirstNoLoopbackIPV4Address());
        request.setSubject("测试扫码支付商品名称");
        request.setBody("测试扫码支付商品描述");
        FbankPrepayResponse response = fbankApiService.prepay(request);
        System.out.println("聚合支付接口响应报文：" + JSON.toJSONString(response));

    }

    /**
     * 聚合支付测试(被扫)
     */
    @Test
    public void testPrepay50() {
        FbankPrepayRequest request = new FbankPrepayRequest();
        request.setMchntOrderNo(Ids.oid());
        request.setPayPowerId("50");//42：公众号，50：被扫，51主扫
        request.setAmount("1");
        request.setAuthCode("135700129694582583");
        request.setTerminalId("wap00001");
        request.setClientIp(IPUtil.getFirstNoLoopbackIPV4Address());
        request.setSubject("测试被扫支付商品名称");
        request.setBody("测试被扫支付商品描述");
        FbankPrepayResponse response = fbankApiService.prepay(request);
        System.out.println("聚合支付接口响应报文：" + JSON.toJSONString(response));

    }

    /**
     * 聚合支付测试(公众号)
     */
    @Test
    public void testPrepay42() {
        FbankPrepayRequest request = new FbankPrepayRequest();
        request.setMchntOrderNo(Ids.oid());
        request.setPayPowerId("42");//42：公众号，50：被扫，51主扫
        request.setAmount("1");
        request.setSubOpenId("oJ6bEwcKMRd9MLRngLh01g7b9DsI");
        request.setSubAppId("wx16b1acc380cad70e");
        request.setClientIp(IPUtil.getFirstNoLoopbackIPV4Address());
        request.setSubject("测试被扫支付商品名称");
        request.setBody("测试被扫支付商品描述");
        FbankPrepayResponse response = fbankApiService.prepay(request);
        System.out.println("聚合支付接口响应报文：" + JSON.toJSONString(response));

    }

    /**
     * 交易退款
     */
    @Test
    public void testTradeRefund() {
        FbankTradeRefundRequest request = new FbankTradeRefundRequest();
        request.setMchntOrderNo("o18091115363772680001");
        request.setReason("任性想退");
        request.setRefMchntOrderNO(Ids.oid());
        request.setRefundFee("1");
        FbankTradeRefundResponse response = fbankApiService.tradeRefund(request);
        System.out.println("交易退款接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 交易退款查询
     */
    @Test
    public void testTradeRefundQuery() {
        FbankTradeRefundQueryRequest request = new FbankTradeRefundQueryRequest();
        request.setRefMchntOrderNo("o18091115381171880001");
//        request.setMchntOrderNo("o18091115363772680001");
        request.setOrderNo("15366513976982762308");
        FbankTradeRefundQueryResponse response = fbankApiService.tradeRefundQuery(request);
        System.out.println("交易退款查询接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 交易撤销
     */
    @Test
    public void testTradeCancel() {
        FbankTradeCancelRequest request = new FbankTradeCancelRequest();
        request.setMchntOrderNo("o18091015414352600001");
        FbankTradeCancelResponse response = fbankApiService.tradeCancel(request);
        System.out.println("交易撤销接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 交易关闭
     */
    @Test
    public void testTradeClose() {
        FbankTradeCloseRequest request = new FbankTradeCloseRequest();
        request.setMchntOrderNo("o18091010202539680001");
        FbankTradeCloseResponse response = fbankApiService.tradeClose(request);
        System.out.println("交易关闭接口响应报文：" + JSON.toJSONString(response));
    }


    /**
     * 查询用户信息
     */
    @Test
    public void testTradeQuery() {
        FbankTransQueryRequest request = new FbankTransQueryRequest();
        request.setMchntOrderNo("o18091015473731280001");
        FbankTransQueryResponse response = fbankApiService.transQuery(request);
        System.out.println("订单查询接口响应报文：" + JSON.toJSONString(response));
    }

    /**
     * 测试查询下载对账文件
     */
    @Test
    public void testReconFileUrlQuery() {
        FbankReconFileUrlQueryRequest request = new FbankReconFileUrlQueryRequest();
        int period = 20180811;
        for (int i = 0; i < 10; i++) {
            request.setOrderDt(String.valueOf(period + i));
            FbankReconFileUrlQueryResponse response = fbankApiService.reconFileUrlQuery(request);
            System.out.println("查询下载对账文件：" + JSON.toJSONString(response));
        }
    }
}
