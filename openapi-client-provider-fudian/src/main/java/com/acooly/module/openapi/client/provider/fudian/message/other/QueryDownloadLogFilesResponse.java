/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:55:05 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.other;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianResponse;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:55:05
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.QUERY_DOWNLOADLOGFILES ,type = ApiMessageType.Response)
public class QueryDownloadLogFilesResponse extends FudianResponse {

    /**
     * 下载日期
     * 指定日期查询流水记录YyyyMMdd
     */
    @NotEmpty
    @Length(min = 8,max=8)
    private String queryDate;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 文件目录
     * 对账文件再SFTP服务器上的地址目录
     */
    @NotEmpty
    @Length(max=256)
    private String sftpFilePath;

    /**
     * 文件名称
     * 对账文件名称
     */
    @NotEmpty
    @Length(max=256)
    private String filename;

    /**
     * 交易类型
     * 充值:recharge，提现:withDraw，标的投标:invest，借款人还款:repayment，投资人回款:loanBack，债权认购:creditInvest，满标放款：loanFull
     */
    @NotEmpty
    @Length(max=32)
    private String type;
}