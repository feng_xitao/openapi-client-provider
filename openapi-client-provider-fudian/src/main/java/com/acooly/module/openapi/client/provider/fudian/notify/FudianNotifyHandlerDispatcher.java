/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-26 10:53 创建
 */
package com.acooly.module.openapi.client.provider.fudian.notify;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.api.notify.AbstractSpringNotifyHandlerDispatcher;
import com.acooly.module.openapi.client.provider.fudian.FudianApiServiceClient;
import com.acooly.module.openapi.client.provider.fudian.FudianConstants;
import com.acooly.module.openapi.client.provider.fudian.OpenAPIClientFudianProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Fudian 专用异步通知分发器
 *
 * @author zhangpu 2017-09-26 10:53
 */
@Component
public class FudianNotifyHandlerDispatcher extends AbstractSpringNotifyHandlerDispatcher {

    @Autowired
    private FudianApiServiceClient fudianApiServiceClient;

    @Autowired
    protected OpenAPIClientFudianProperties openAPIClientFudianProperties;

    @Override
    protected String getServiceKey(String notifyUrl, Map<String, String> notifyData) {
        return getServiceName(notifyUrl);
    }

    @Override
    protected ApiServiceClient getApiServiceClient() {
        return this.fudianApiServiceClient;
    }


    private String getServiceName(String notifyUrl) {
        String canonicalUrl = FudianConstants.getCanonicalUrl("/",
                openAPIClientFudianProperties.getNotifyUrl());
        String serviceName = Strings.substringAfter(notifyUrl, canonicalUrl);
        if (!Strings.startsWith(serviceName, "/")) {
            serviceName = "/" + serviceName;
        }
        return serviceName;
    }
}
