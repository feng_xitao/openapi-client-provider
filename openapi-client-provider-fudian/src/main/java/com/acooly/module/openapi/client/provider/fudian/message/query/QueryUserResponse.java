/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:32:16 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.query;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianResponse;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:32:16
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.QUERY_USER ,type = ApiMessageType.Response)
public class QueryUserResponse extends FudianResponse {

    /**
     * 账户号
     * 用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max=50)
    private String accountNo;

    /**
     * 授权结果
     * {“loanInvest”:”0000”
     */
    @NotEmpty
    @Length(max=256)
    private String authorization;

    /**
     * 账户可用余额
     * 账户可用余额，以元为单位
     */
    @NotEmpty
    @Length(max=20)
    private String balance;

    /**
     * 可提现余额
     * 可以提现的余额
     */
    @NotEmpty
    @Length(max=20)
    private String withDrawbalance;

    /**
     * 账户冻结余额
     * 账户冻结金额，以元为单位
     */
    @NotEmpty
    @Length(max=20)
    private String freezeBalance;

    /**
     * 证件号码
     * 证件号码
     */
    @NotEmpty
    @Length(max=32)
    private String identityCode;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 账户状态
     * 1代表账户状态正常，2代表账户冻结，3代表账户挂失，4账户销户
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String status;
}