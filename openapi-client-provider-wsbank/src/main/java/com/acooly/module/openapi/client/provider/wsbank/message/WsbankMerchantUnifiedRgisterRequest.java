package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankRequest;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankMerchantUnifiedRgisterRequestInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author sunjx 2018/5/23 14:00
 */
@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.MERCHANT_UNIFIED_RGISTER,type = ApiMessageType.Request)
public class WsbankMerchantUnifiedRgisterRequest extends WsbankRequest {

    @XStreamAlias("request")
    private WsbankMerchantUnifiedRgisterRequestInfo requestInfo;
    
    @Override
    public void doCheck() {
        Validators.assertJSR303(requestInfo);
        Validators.assertJSR303(requestInfo.getHeadRequest());
        Validators.assertJSR303(requestInfo.getRequestBody());
        requestInfo.getRequestBody().doCheck();
    }
}
