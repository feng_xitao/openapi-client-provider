package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class FundDetails implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
     * 额度
     */
    @JSONField(name = "Amount")
    private String amount;

    /**
     * 币种
     */
    @JSONField(name = "Currency")
    private String currency = "CNY";

    /**
     * 参与方id
     * 如果是商户，则填入merchantId,如果是平台，则填入isvOrgId
     */
    @JSONField(name = "ParticipantId")
    private String participantId;

    /**
     * 参与方类型
     * 商户(merchant)、平台(platform)
     */
    @JSONField(name = "ParticipantType")
    private String participantType;

    /**
     * 用途	String	32	C	货款：LOAN; 服务费：FEE；运费：FREIGHT;
     */
    @JSONField(name = "Purpose")
    private String purpose;

    /**
     * 扩展信息
     */
    @JSONField(name = "ExtInfo")
    private String extInfo;

    /**
     * 备注
     */
    @JSONField(name = "Remark")
    private String remark;
}
