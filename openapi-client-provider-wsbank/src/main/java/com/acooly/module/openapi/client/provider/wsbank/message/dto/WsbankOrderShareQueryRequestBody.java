package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsbankOrderShareQueryRequestBody implements Serializable {

    /**
     * 合作方机构号（网商银行分配）
     */
    @Size(max = 32)
    @XStreamAlias("IsvOrgId")
    @NotBlank
    private String isvOrgId;

    /**
     * 收款方商户号
     */
    @Size(max = 32)
    @XStreamAlias("MerchantId")
    @NotBlank
    private String merchantId;

    /**
     * 关联网商订单号
     */
    @Size(max = 32)
    @XStreamAlias("RelateOrderNo")
    @NotBlank
    private String relateOrderNo;

    /**
     * 外部订单分账请求流水号
     */
    @Size(max = 64)
    @XStreamAlias("OutTradeNo")
    @NotBlank
    private String outTradeNo;

    /**
     * 分账单号
     */
    @Size(max = 64)
    @XStreamAlias("ShareOrderNo")
    @NotBlank
    private String shareOrderNo;
}
