package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.acooly.module.openapi.client.provider.wsbank.message.base.WsbankHeadRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("request")
public class WsBankBkCloudFundsMerchantOpenPayRequestInfo implements Serializable {

	private static final long serialVersionUID = 61024238690523491L;

	/**
     * 请求报文头
     */
    @XStreamAlias("head")
    private WsbankHeadRequest headRequest;

    /**
     * 请求报文体
     */
    @XStreamAlias("body")
    private WsBankBkCloudFundsMerchantOpenPayRequestBody requestBody;
}
