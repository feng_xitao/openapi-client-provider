/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2018-01-24 17:39 创建
 */
package com.acooly.module.openapi.client.provider.wsbank.service;

import com.acooly.module.openapi.client.provider.wsbank.OpenAPIClientWsbankProperties;
import com.acooly.module.openapi.client.provider.wsbank.WsbankConstants;
import com.acooly.module.safety.key.AbstractKeyLoadManager;
import com.acooly.module.safety.key.KeyPairLoader;
import com.acooly.module.safety.support.KeyPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author zhike 2018-01-24 17:39
 */
@Component
public class WsbankKeyLoader extends AbstractKeyLoadManager<KeyPair> implements KeyPairLoader {

    @Autowired
    protected OpenAPIClientWsbankProperties properties;

    @Override
    public KeyPair doLoad(String principal) {
        KeyPair keyPair = new KeyPair(properties.getPublicKey(),properties.getPrivateKey());
        keyPair.setSignatureAlgo("SHA256withRSA");
        // 最后load下，内部会缓存。
        keyPair.loadKeys();
        return keyPair;
    }

    @Override
    public String getProvider() {
        return WsbankConstants.PROVIDER_NAME;
    }
}
