/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.provider.yl.marshall;


import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.provider.yl.domain.YlRequest;

import org.springframework.stereotype.Service;

/**
 * @author fufeng
 */
@Service
public class YlRequestMarshall extends YlMarshallSupport implements ApiMarshal<String, YlRequest> {

    @Override
    public String marshal(YlRequest source) {
        beforeMarshall(source);
        return doMarshall(source);
    }

}
