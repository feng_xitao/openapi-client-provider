package com.acooly.module.openapi.client.provider.cj.message;

import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.cj.domain.CjApiMsgInfo;
import com.acooly.module.openapi.client.provider.cj.domain.CjRequest;
import com.acooly.module.openapi.client.provider.cj.enums.CjServiceEnum;
import com.acooly.module.openapi.client.provider.cj.message.dto.CjDeductBodyInfo;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@CjApiMsgInfo(service = CjServiceEnum.CJ_BATCH_DEDUCT, type = ApiMessageType.Request)
public class CjBatchDeductRequest extends CjRequest {

    /** 业务代码, 接入生产前，业务人员会提供 */
    private String businessCode;
    /** 产品编码, 接入生产前，业务人员会提供 */
    private String productCode;
    /** 企业账号 */
    private String corpAccNo;
    /** 对公对私 */
    private String accountProp;
    /** 支付时效性 */
    private String timeliness;
    /** 预约清算日期 */
    private String appointmentTime;
    /** 总记录数 */
    private int totalCnt;
    /** 总金额 */
    private Money totalAmt;

    List<CjDeductBodyInfo> cjDeductBodyInfos;

}
